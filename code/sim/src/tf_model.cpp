// tf_model.cpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "liblammpsplugin.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::stringstream;
using std::string;
using std::vector;

int main(int argc, char* argv[]) {

  if (argc != 3) {
    cout << "Usage: tf_model lmpPlugin paramsFile" << endl;
    return 1;
  }

  // Read parameters
  int argi = 0;
  string lmpPlugin (argv[++argi]);
  string paramsFile (argv[++argi]);

  liblammpsplugin_t* plugin = liblammpsplugin_load(lmpPlugin.c_str());
  if (plugin == nullptr) {
    cout << "Error: cannot load the shared LAMMPS library" << endl;
    return 1;
  }

  ifstream reader;
  reader.open(paramsFile);
  if (!reader) {
    cout << "Error: cannot open the file " << paramsFile << endl;
  }
  string line, str;
  stringstream ss;
  int nargs = 0;
  int runLoopTime = 0;
  int nloops = 0;
  string equilLam, switchLam, mainLam, finalLam;
  string logFile = "none";
  string screenFile = "none";
  
  while (getline(reader, line)) {
    // Skip comments and empty lines
    if (line.size() == 0 || line[0] == '#') continue;
    ss.clear();
    ss.str(line);
    ss >> str;
    if (str == "run_loop_time") {
      ss >> runLoopTime;
      nargs++;
    } else if (str == "nloops") {
      ss >> nloops;
      nargs++;
    } else if (str == "equil_lam") {
      ss >> equilLam;
      nargs++;
    } else if (str == "main_lam") {
      ss >> mainLam;
      nargs++;
    } else if (str == "switch_lam") {
      ss >> switchLam;
      nargs++;
    } else if (str == "final_lam") {
      ss >> finalLam;
      nargs++;
    } else if (str == "log_file") {
      ss >> logFile;
    } else if (str == "screen_file") {
      ss >> screenFile;
    } else {
      cout << "Error: unrecognised parameter " << str << endl;
      return 1;
    }
  }
  reader.close();

  if (nargs != 6) {
    cout << "Error: not all required parameters specified" << endl;
    return 1;
  }
  
  // Create LAMMPS instance
  void *lmp; // LAMMPS handle
  vector<char*> lmpargv;
  if (screenFile == "STDOUT") {
    lmpargv = {(char*) "liblammps", (char*) "-log", &logFile[0]};
  } else {
    lmpargv = 
      {(char*) "liblammps", (char*) "-log", &logFile[0], (char*) "-screen",
       &screenFile[0]};
  }
  lmp = plugin->open_no_mpi((int) lmpargv.size(), lmpargv.data(), nullptr);
  if (lmp == nullptr) {
    cout << "LAMMPS initialization failed" << endl;
    plugin->mpi_finalize();
    return 1;
  }

  // Do init equilibration
  plugin->file(lmp, &equilLam[0]);

  // Do main simulation
  plugin->file(lmp, &mainLam[0]);

  // Command for the run in each loop iteration
  ss.clear(); ss.str("");
  ss << "run " << runLoopTime << " post no\n";
  string runcmd = ss.str();
  
  for (int n = 0; n < nloops; n++) {    
    // Do protein switching
    plugin->file(lmp, &switchLam[0]);

    // Do the run
    plugin->commands_string(lmp, runcmd.c_str());
  } // Close run loops
  
  // Delete LAMMPS isntance and shut down MPI
  plugin->close(lmp);
  plugin->mpi_finalize();
  return 0;
}
