#!/bin/env python3
# tf_model.py

import sys
import numpy as np
from lammps import lammps

args = sys.argv

if (len(args) != 2):
    print("Usage: tf_model.py params_file")
    sys.exit(1)

params_file = args.pop(1)

########################################

# Read params file

nargs = 0
npolybeads = None
run_loop_time = None
nloops = None
equil_lam = None
switch_lam = None
main_lam = None
log_file = None
screen_file = None

with open(params_file,'r') as reader:
    for line in reader:
        if (len(line) == 0 or line.startswith('#')): continue
        args = line.split()
        if (len(args) == 0): continue
        if (len(args) < 2):
            print("Error: not enough arguments")
            sys.exit(1)
        field, value = args[0], args[1]
        if (field == "run_loop_time"):
            run_loop_time = int(value)
            nargs += 1
        elif (field == "nloops"):
            nloops = int(value)
            nargs += 1
        elif (field == "equil_lam"):
            equil_lam = value
            nargs += 1
        elif (field == "main_lam"):
            main_lam = value
            nargs += 1
        elif (field == "switch_lam"):
            switch_lam = value
            nargs += 1            
        elif (field == "final_lam"):
            final_lam = value
            nargs += 1
        elif (field == "log_file"):
            log_file = value
        elif (field == "screen_file"):
            screen_file = value    
        else:
            print("Error: unrecognised parameter %s" % field)
            sys.exit(1)

if (nargs != 6):
    print("Error: not all required arguments specified")
    sys.exit(1)

########################################

# Run the simulation
if (log_file == None): log_file = "none"
cargs = ["-log", log_file]
if (screen_file == None): screen_file = "none"
if (screen_file != "STDOUT"):
    cargs += ["-screen", screen_file]
lmp = lammps(cmdargs=cargs)

# Do init equilibriation
lmp.file(equil_lam)

# Do main simulation
lmp.file(main_lam)

# Command for the run in each loop iteration
runcmd = "run {:d} post no".format(run_loop_time)

for n in range(0,nloops):
    # Do switching
    lmp.file(switch_lam)
    
    # Do the run
    lmp.command(runcmd)

# Finalise and clean up
lmp.file(final_lam)
